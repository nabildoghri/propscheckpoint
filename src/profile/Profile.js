import PropTypes from "prop-types";

function Profile(props){
    return <div style={{backgroundColor: "lightgray"}}>
        <div className="col-12 justify-content-md-center" >
            {props.children}
        </div>
        <div className="row justify-content-md-center" style={{backgroundColor: "gray", margin:"2px", padding:"10px"}} onClick={props.handleClick}>
            <div className="col col-lg-4" style={{color: "white"}}>Name :</div>
            <div className="col col-lg-8">{props.fullName}</div>
        </div>
        <div className="row justify-content-md-center" style={{backgroundColor: "gray", margin:"2px", padding:"10px"}}>
        <div className="col col-lg-4" style={{color: "white"}}>Bio :</div>
        <div className="col-8" >{props.bio}</div>
        </div>
        <div className="row justify-content-md-center" style={{backgroundColor: "gray", margin:"2px", padding:"10px"}}>
        <div className="col col-lg-4" style={{color: "white"}} >Profession :</div>
        <div className="col-8">{props.profession}</div>
        </div>
        
        
    </div>
}
Profile.propTypes = {
    fullName: PropTypes.string,
    bio: PropTypes.string,
    profession: PropTypes.string,
    handleName: PropTypes.func,
   };
Profile.defaultProps = {
    fullName: 'jon Snow',
    bio : "valar Morgholis",
    profession: " bastard of winterfel "
};
export default Profile;